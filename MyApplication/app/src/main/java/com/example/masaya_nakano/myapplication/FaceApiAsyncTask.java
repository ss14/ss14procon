package com.example.masaya_nakano.myapplication;

import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * FaceAPIの非同期実行用クラス
 */
public class FaceApiAsyncTask extends AsyncTask<ResultInfo, Void, Object> {

    private MainFragment fragment;

    public FaceApiAsyncTask(MainFragment fragment) {
        this.fragment = fragment;
    }

    //③バックグラウンド処理
    @Override
    protected Object doInBackground(ResultInfo[] data) {
        //並列処理
        ExecutorService service = Executors.newFixedThreadPool(data.length);
        List<FaceApiDto> faceApiResultDtoList = new ArrayList();
        for(ResultInfo resultInfo : data){
            FaceApiDto dto = new FaceApiDto();
            dto.setImage(resultInfo.getImage());
            dto.setFuture(service.submit(new FaceApiService(resultInfo.image)));

            faceApiResultDtoList.add(dto);
        }

        List<String> resultList= new ArrayList<String>();
        try {
            // 取得結果を副作用で設定
            for(FaceApiDto dto : faceApiResultDtoList){
                Future<String> future = dto.getFuture();
                dto.setResultJson(future.get());
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return faceApiResultDtoList;
    }

    //⑩バックグラウンド完了処理
    @Override
    protected void onPostExecute(Object result) {
        super.onPostExecute(result);

        // UI反映用CallBack呼び出し
        fragment.callBack(( List<FaceApiDto>)result);
    }
}
