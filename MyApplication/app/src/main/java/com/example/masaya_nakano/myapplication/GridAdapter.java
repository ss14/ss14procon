package com.example.masaya_nakano.myapplication;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * 画像表示GridAdapter
 */
public class GridAdapter extends BaseAdapter implements View.OnTouchListener {

    private Activity mActivity;
    private MainFragment mMainFragment;
    private static final int REQUEST_CODE = 1;

    final String[] items = {"カメラ", "アルバムから選択する"};

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        new AlertDialog.Builder(mActivity)
                .setTitle("写真を選択してください")
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            mMainFragment.takePicture();
                        } else {
                            mMainFragment.choosePhoto();
                        }
                    }
                })
                .setNegativeButton("キャンセル", null)
                .show();
        return false;
    }

    class ViewHolder {
        ImageView imageView;
        ImageButton deleteButton;
    }

    private List<Bitmap> bitmapList = new ArrayList<>();
    private LayoutInflater inflater;
    private int layoutId;

    // 引数がMainActivityからの設定と合わせる
    GridAdapter(
                int layoutId,
                List<Bitmap> bList,
                MainFragment fragment) {
        super();
        mMainFragment = fragment;
        mActivity = fragment.getActivity();
        this.inflater = (LayoutInflater)
                mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.layoutId = layoutId;
        setBitmap(bList);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            // main.xml の <GridView .../> に grid_items.xml を inflate して convertView とする
            convertView = inflater.inflate(layoutId, parent, false);
            // ViewHolder を生成
            holder = new ViewHolder();

            holder.imageView = convertView.findViewById(R.id.image_view);
            holder.deleteButton = convertView.findViewById(R.id.delete_button);


            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.imageView.setImageBitmap(bitmapList.get(position));
        // 末尾の画像のみ画像追加ダイアログタッチイベントを有効化
        if(position == bitmapList.size() - 1){
            holder.imageView.setOnTouchListener(this);
            holder.deleteButton.setVisibility(View.INVISIBLE);
        }
        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bitmapList.remove(position);
                mMainFragment.removeImage(position);
                mMainFragment.reloadView();
            }
        });

        return convertView;
    }

    @Override
    public int getCount() {
        // List<String> imgList の全要素数を返す
        return bitmapList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public void setBitmap(List<Bitmap> val){
        bitmapList = val;
        bitmapList.add(BitmapFactory.decodeResource(mMainFragment.getResources(), R.drawable.man2));
    }
}

