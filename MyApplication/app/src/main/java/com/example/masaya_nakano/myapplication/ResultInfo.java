package com.example.masaya_nakano.myapplication;

import android.graphics.Bitmap;
import android.net.Uri;

public class ResultInfo {

    Bitmap image;
    String name;
    String money;

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }
}

