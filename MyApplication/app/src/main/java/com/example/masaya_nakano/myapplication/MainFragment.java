package com.example.masaya_nakano.myapplication;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * メイン画面
 */
public class MainFragment extends Fragment implements View.OnClickListener , AdapterView.OnItemClickListener, TextWatcher{

    private static final int CHOOSE_PHOTO_REQUEST_CODE = 1234;
    private static final int RESULT_CAMERA = 1001;

    private int mAmount = 0;

    private Dialog mDialog;
    private View mView;
    private EditText mEditText;
    private List<ResultInfo> selectedImageList = new ArrayList<ResultInfo>();
    private GridView mGridView;

    /**
     * 割り勘スタートボタン
     */
    private Button mShareButton;

    // Resource IDを格納するarray
    private List<Integer> imgList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_main, container, false);

        int buttonColor = getContext().getResources().getColor(android.R.color.darker_gray);
        mShareButton = mView.findViewById(R.id.share_start_button);
        mShareButton.setOnClickListener(this);
        mShareButton.setEnabled(false);
        mShareButton.setBackgroundColor(buttonColor);

        mEditText = mView.findViewById(R.id.input_money);
        mEditText.addTextChangedListener(this);
        // for-each member名をR.drawable.名前としてintに変換してarrayに登録
        List temp = new ArrayList<Bitmap>();

        // 選択済みBitMapに追加
        for(ResultInfo item : selectedImageList){
            temp.add(item.getImage());
        }

        // GridViewのインスタンスを生成
        mGridView = mView.findViewById(R.id.grid_view);
        // BaseAdapter を継承したGridAdapterのインスタンスを生成
        // 子要素のレイアウトファイル grid_items.xml を
        // activity_main.xml に inflate するためにGridAdapterに引数として渡す
        GridAdapter adapter = new GridAdapter(
                R.layout.grid_item,
                temp,
                this
        );

        // item clickのListnerをセット
        mGridView.setOnItemClickListener(this);
        // gridViewにadapterをセット
        mGridView.setAdapter(adapter);
        return mView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    }

    /**
     * 割り勘ボタン押下時処理
     * @param v
     */
    @Override
    public void onClick(View v) {
        String mAmountStr = mEditText.getText().toString();

        try{
            mAmount = Integer.parseInt(mAmountStr);
        }catch (NumberFormatException e){
            new AlertDialog.Builder(this.getActivity())
                    .setMessage("金額の値が大きすぎます")
                    .setPositiveButton("閉じる", null)
                    .show();
            return;
        }

        // FaceAPIを投げる
        ApiClient apiClient = new ApiClient(this);
        apiClient.postRequest(selectedImageList);

        mDialog = LoadingDialog.getDialog(this.getActivity());
        mDialog.show();
    }

    @Override
    public void onPause(){
        super.onPause();
        if(mDialog != null && mDialog.isShowing()){
            mDialog.dismiss();
        }
    }

    /**
     * FaceAPIの結果をUIに反映させるためのCallBackメソッド
     * FaceAPI終了後に呼ばれる
     * @param result FaceAPIの結果
     */
    public void callBack(List<FaceApiDto> result){
        List<ResultInfo> resultInfoList = Calc.exec(result, mAmount);
        // 副作用で名前を設定
        for (ResultInfo resultInfo : resultInfoList) {
            String name = getRandomName1() + getRandomName2();
            resultInfo.setName(name);
        }

        // 結果画面に引数を設定
        Fragment resultFragment = new ResultFragment();
        Bundle args = new Bundle();
        args.putSerializable("resultInfo", resultInfoList.toArray());
        resultFragment.setArguments(args);

        // 結果画面へ遷移
        FragmentManager mFragmentManager = getFragmentManager();
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.replace(R.id.top_layout, resultFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    /**
     * 画像追加ダイアログの選択後処理
     * @param requestCode
     * @param resultCode
     * @param resultData
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        super.onActivityResult(requestCode, resultCode, resultData);
        // 選択したものにより、処理を分岐
        if (requestCode == CHOOSE_PHOTO_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            // 結果がnullの場合何も処理しない
            if (resultData == null) {
                return;
            }

            // フォトから選択
            if (resultData.getClipData() != null) {
                int itemCount = resultData.getClipData().getItemCount();
                List uriList = new ArrayList<Uri>();
                for (int i = 0; i < itemCount; i++) {
                    Uri uri = resultData.getClipData().getItemAt(i).getUri();
                    if (uri != null) {
                        uriList.add(uri);
                        try {
                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContext().getContentResolver(), uri);

                            // 選択済みBitmapに追加
                            ResultInfo resultInfo = new ResultInfo();
                            resultInfo.setImage(bitmap);

                            selectedImageList.add(resultInfo);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else if (resultData.getData() != null) { //画像から選択？
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContext().getContentResolver(), resultData.getData());

                    // 選択済みBitmapに追加
                    ResultInfo resultInfo = new ResultInfo();
                    resultInfo.setImage(bitmap);

                    selectedImageList.add(resultInfo);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if(requestCode == RESULT_CAMERA && resultCode == Activity.RESULT_OK) {
            if(resultData.getExtras() != null) {
                Bitmap bitmap = (Bitmap)resultData.getExtras().get("data");

                // 選択済みBitmapに追加
                ResultInfo resultInfo = new ResultInfo();
                resultInfo.setImage(bitmap);

                selectedImageList.add(resultInfo);
            }
        }

        // GridViewのインスタンスを生成
        mGridView = mView.findViewById(R.id.grid_view);
        GridAdapter mAdapter = (GridAdapter) mGridView.getAdapter();

        // BaseAdapter を継承したGridAdapterのインスタンスを生成
        // 子要素のレイアウトファイル grid_items.xml を
        // activity_main.xml に inflate するためにGridAdapterに引数として渡す
        List bitmapList = new ArrayList<Bitmap>();

        // 選択済みBitMapに追加
        for(ResultInfo item : selectedImageList){
            bitmapList.add(item.getImage());
        }

        mAdapter.setBitmap(bitmapList);
        mGridView.setAdapter(mAdapter);
        activeDecisionShareButton();
    }

    /**
     * カメラ選択
     */
    public void choosePhoto() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        this.startActivityForResult(Intent.createChooser(intent, "Choose Photo"),
                CHOOSE_PHOTO_REQUEST_CODE);
    }

    /**
     * 写真選択
     */
    public void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, RESULT_CAMERA);
    }

    /**
     * テキスト変更時処理
     * @param s
     * @param start
     * @param before
     * @param count
     */
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        activeDecisionShareButton();
    }

    /**
     * 割り勘開始ボタンのアクティブ判定
     */
    private void activeDecisionShareButton(){
        String mAmountStr = mEditText.getText().toString();
        int buttonColor;
        if (mAmountStr.isEmpty() || mAmountStr == "0" || selectedImageList.size() == 0) {
            mShareButton.setEnabled(false);
            buttonColor = getContext().getResources().getColor(android.R.color.darker_gray);
        } else {
            mShareButton.setEnabled(true);
            buttonColor = getContext().getResources().getColor(android.R.color.holo_green_dark);
        }
        mShareButton.setBackgroundColor(buttonColor);
    }

    /*
    金額入力検知
    */
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }
    @Override
    public void afterTextChanged(Editable s) {
    }

    /**
     * 画像の削除
     * @param position
     */
    public  void removeImage(int position){
        selectedImageList.remove(position);
    }

    /**
     * Viewの再読み込み
     */
    public void reloadView() {
        GridAdapter mAdapter = (GridAdapter) mGridView.getAdapter();
        mAdapter.notifyDataSetChanged();
        mGridView.setAdapter(mAdapter);
        activeDecisionShareButton();
    }

    /**
     * 名前生成1
     * @return
     */
    private String getRandomName1(){
        String name = "";

        String[] names = getContext().getResources().getStringArray(R.array.name1);
        int randomNum = new Random().nextInt(names.length);
        name = names[randomNum];

        return name;
    }

    /**
     * 名前生成2
     * @return
     */
    private String getRandomName2(){
        String name = "";

        String[] names = getContext().getResources().getStringArray(R.array.name2);
        int randomNum = new Random().nextInt(names.length);
        name = names[randomNum];

        return name;
    }
}
