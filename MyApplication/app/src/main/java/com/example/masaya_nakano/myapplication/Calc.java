package com.example.masaya_nakano.myapplication;

import android.graphics.Bitmap;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 割り勘計算用クラス
 */
public class Calc {

    /**
     * 計算用一時クラス
     */
    private static class TempResultData{
        /** 計算対象の画像 */
        public Bitmap image;
        /** 計算対象の数値 */
        public int calcSource;
    }

    /** 割り勘単位 */
    private static final int DIGIT = 100;

    /**
     * 年齢から金額計算
     * @param resultInfoList
     * @param amount
     * @return
     */
    public static List<ResultInfo> exec(List<FaceApiDto> resultInfoList, int amount){
        List<TempResultData> tempDataList = new ArrayList<>();
        double sum = 0;
        for(FaceApiDto item : resultInfoList){
            int age;
            try {
                JSONArray jsonObject = new JSONArray(item.getReslutJson());
                age = jsonObject.getJSONObject(0).getJSONObject("faceAttributes").getInt("age");
            } catch (Exception e) { // 例外発生時、適当な値を設定
                age =  new Random().nextInt(50) + 20;
                e.printStackTrace();
            }
            // 合計値に加算
            sum += age;

            TempResultData tempData = new TempResultData();
            tempData.calcSource = age;
            tempData.image = item.getImage();
            tempDataList.add(tempData);
        }

        // あまり
        double surplus = 0;
        // 計算結果のリスト
        List<ResultInfo> resultList = new ArrayList<>();
        for(int i = 0; i < tempDataList.size(); i++){
            TempResultData item  = tempDataList.get(i);

            double temp =(item.calcSource/ sum * amount);
            double tempSurplus = temp % DIGIT;
            surplus += tempSurplus;
            int money = (int)(temp - tempSurplus);
            if(i == resultInfoList.size() -1 ){
                money += Math.round(surplus);
            }

            ResultInfo result = new ResultInfo();
            result.image = item.image;
            result.money =  money +"円";
            resultList.add(result);
        }

        return  resultList;
    }

    /**
     * 画像サイズによる適当な金額計算
     * @param resultInfoList
     * @param amount
     * @return
     */
    private static  List<ResultInfo> bitmapCalc(List<FaceApiDto> resultInfoList, int amount){
        List<ResultInfo> resultList = new ArrayList<>();

        double sum = 0;
        for(FaceApiDto item : resultInfoList){
            sum += item.getImage().getByteCount();
        }

        double surplus = 0;

        for(int i = 0; i < resultInfoList.size(); i++){
            FaceApiDto item  = resultInfoList.get(i);

            ResultInfo result = new ResultInfo();
            result.name = item.getReslutJson();
            result.image = item.getImage();

            double temp =(item.getImage().getByteCount()/ sum * amount);
            double tempSurplus = temp % DIGIT;
            surplus += tempSurplus;
            int money = (int)(temp - tempSurplus);
            if(i == resultInfoList.size() -1 ){
                money += Math.round(surplus);
            }
            result.money =  money +"円";
            resultList.add(result);
        }

        return  resultList;
    }
}
