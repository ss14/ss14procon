package com.example.masaya_nakano.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ResultListAdapter extends BaseAdapter {

    Context context;
    LayoutInflater layoutInflater = null;
    List<ResultInfo> resultList;

    public ResultListAdapter(Context context) {
        this.context = context;
        this.layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setResultList(List<ResultInfo> resultList) {
        this.resultList = resultList;
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public Object getItem(int position) {
        return resultList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return resultList.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = layoutInflater.inflate(R.layout.result_item,parent,false);

        ((ImageView)convertView.findViewById(R.id.imageView)).setImageBitmap(resultList.get(position).getImage());
        ((TextView)convertView.findViewById(R.id.name)).setText(resultList.get(position).getName());
        ((TextView)convertView.findViewById(R.id.money)).setText(resultList.get(position).getMoney());

        return convertView;
    }
}

