package com.example.masaya_nakano.myapplication;

import java.io.IOException;
import java.util.List;

/**
 * 踏み台的な何か
 * 特に意味ないかも
 */
public class ApiClient {

    private MainFragment fragment;

    public  ApiClient(MainFragment fragment){
        this.fragment = fragment;
    }

    /**
     * POSTリクエストベタ打ち版
     * @throws IOException
     */
    public  void postRequest(final List<ResultInfo> mResultInfoList) {
        // FaceAPIを投げる
        // UIスレッドに反映するためにMainFragmentをコンストラクタに指定している
        new FaceApiAsyncTask(fragment).execute(mResultInfoList.toArray(new ResultInfo[]{}));
    }
}
