package com.example.masaya_nakano.myapplication;

import android.graphics.Bitmap;
import android.net.Uri;

import java.util.concurrent.Future;

/**
 * FaceAPIのリクエスト、レスポンス周りのまとめたもの
 */
public class FaceApiDto {
    /** リクエスト用Image */
    private Bitmap image;
    /** 非同期処理用 */
    private Future<String> future;
    /** 結果のJson */
    private String resultJson;

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public Future<String> getFuture() {
        return future;
    }

    public void setFuture(Future<String> future) {
        this.future = future;
    }

    public  String getReslutJson(){ return  resultJson; }

    public  void setResultJson(String resultJson){ this.resultJson = resultJson; }
}
