package com.example.masaya_nakano.myapplication;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;


public class TopFragment extends Fragment implements View.OnClickListener{

    private Button startBtn;

    private View mView;

    private FragmentManager mFragmentManager;

    public TopFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_top, container, false);
        mFragmentManager = getFragmentManager();

        ImageView imageView = mView.findViewById(R.id.gif);
        int gif = R.drawable.original;
        Glide.with(this).load(gif).into(imageView);

        startBtn = mView.findViewById(R.id.start);
        startBtn.setOnClickListener(this);
        return mView;
    }

    @Override
    public void onClick(View v) {
        Fragment mainFragment = new MainFragment();
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.replace(R.id.top_layout, mainFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}

