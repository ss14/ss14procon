package com.example.masaya_nakano.myapplication;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * FACE API実行クラス
 */
public class FaceApiService implements Callable<String> {

    // 変更が必要
    private final String subscriptionKey = "05869e1c95e54b44a6344bbbcd9adaa2";
    private static final String uriBase = "https://japaneast.api.cognitive.microsoft.com/face/v1.0/detect";
    private Bitmap data;

    public FaceApiService(Bitmap data) {
        this.data = data;
    }

    @Override
    public String call() {
        Map<String, String> param = new HashMap<>();
        param.put("returnFaceId", "true");
        param.put("returnFaceLandmarks","false");
        //param.put("returnFaceAttributes","age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise"); いろいろいらない
        param.put("returnFaceAttributes","age,smile,emotion");

        //MediaType mediaType = MediaType.parse("application/json; charset=utf-8");
        MediaType mediaType = MediaType.parse("application/octet-stream");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        data.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] jpgArr = baos.toByteArray();

        RequestBody requestBody = RequestBody.create(mediaType, jpgArr);

        final Request request = new Request.Builder()
                .url(createUrl(uriBase, param))
                .header("Ocp-Apim-Subscription-Key", subscriptionKey)
                .post(requestBody)
                .build();
        OkHttpClient client = new OkHttpClient.Builder()
                .build();

        String result = "";
        try {
            Response response = client.newCall(request).execute();//同期呼び出し
            ResponseBody body = response.body();
            result = body.string();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * ベースURLにGetパラメータを付与
     * @param baseUrl ベース
     * @param params param
     * @return GETParam付URL
     */
    private String createUrl(String baseUrl, Map<String, String> params){
        String paramStr = "";
        for (Map.Entry<String, String> param: params.entrySet()) {
            paramStr += param.getKey()  + "=" + param.getValue();

            //区切り文字
            paramStr += "&";
        }

        // 末尾の 区切りを削除 まともな方法が知りたい
        paramStr = paramStr.substring(0, paramStr.length() -1 );

        return baseUrl + "?" + paramStr;
    }
}
