package com.example.masaya_nakano.myapplication;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collector;


/**
 * 結果表示画面
 */
public class ResultFragment extends Fragment {

    private View mView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_result, container, false);
        ListView listView = mView.findViewById(R.id.listView);


        Bundle args = getArguments();
        Object[] arg = (Object[])args.getSerializable("resultInfo");
        ResultInfo[] resultInfo =  Arrays.asList(arg).toArray(new ResultInfo[arg.length]);
        List<ResultInfo> list = Arrays.asList(resultInfo);


        ResultListAdapter adapter = new ResultListAdapter(getActivity());

        adapter.setResultList(list);
        listView.setAdapter(adapter);

        return mView;
    }
}
